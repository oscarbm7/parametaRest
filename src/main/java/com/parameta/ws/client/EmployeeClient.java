package com.parameta.ws.client;

import com.parameta.ws.services.User;
import com.parameta.ws.services.RegisterEmployee;
import com.parameta.ws.services.RegisterEmployeeResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

@Slf4j
public class EmployeeClient extends WebServiceGatewaySupport {

    public RegisterEmployeeResponse registerEmployee(User employeeDTO) {
        log.info("Processing request..");
        RegisterEmployee request = new RegisterEmployee();
        request.setUserRequest(employeeDTO);
        log.info("Processing request.."+request);
        RegisterEmployeeResponse response = null;
        try {
            response = (RegisterEmployeeResponse) getWebServiceTemplate()
                    .marshalSendAndReceive(request);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return response;
    }
}
