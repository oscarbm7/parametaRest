
package com.parameta.ws.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for registerEmployeeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="registerEmployeeResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Greeting" type="{http://services.ws.parameta.com/}employeeDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "registerEmployeeResponse", propOrder = {
    "greeting"
})
@XmlRootElement(name = "registerEmployeeResponse")
public class RegisterEmployeeResponse {

    @XmlElement(name = "Greeting")
    protected User greeting;

    /**
     * Gets the value of the greeting property.
     * 
     * @return
     *     possible object is
     *     {@link User }
     *     
     */
    public User getGreeting() {
        return greeting;
    }

    /**
     * Sets the value of the greeting property.
     * 
     * @param value
     *     allowed object is
     *     {@link User }
     *     
     */
    public void setGreeting(User value) {
        this.greeting = value;
    }

}
