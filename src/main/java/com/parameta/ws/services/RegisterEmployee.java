
package com.parameta.ws.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for registerEmployee complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="registerEmployee"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UserRequest" type="{http://services.ws.parameta.com/}employeeDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "registerEmployee", propOrder = {
    "userRequest"
})
@XmlRootElement(name = "registerEmployee")
public class RegisterEmployee {

    @XmlElement(name = "UserRequest")
    protected User userRequest;

    /**
     * Gets the value of the userRequest property.
     * 
     * @return
     *     possible object is
     *     {@link User }
     *     
     */
    public User getUserRequest() {
        return userRequest;
    }

    /**
     * Sets the value of the userRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link User }
     *     
     */
    public void setUserRequest(User value) {
        this.userRequest = value;
    }

}
