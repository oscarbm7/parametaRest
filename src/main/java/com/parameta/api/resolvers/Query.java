package com.parameta.api.resolvers;


import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.parameta.api.model.UserDTO;
import com.parameta.api.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Query implements GraphQLQueryResolver {


    @Autowired
    private UserService userService;

    public UserDTO register(UserDTO userDto) {
        return userService.employeeRegister(userDto);
    }

}
