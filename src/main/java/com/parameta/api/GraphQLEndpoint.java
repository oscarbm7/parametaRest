package com.parameta.api;

import com.coxautodev.graphql.tools.SchemaParser;
import com.parameta.api.resolvers.Query;
import graphql.servlet.SimpleGraphQLServlet;
import javax.servlet.annotation.WebServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@WebServlet(urlPatterns = "/employee")
public class GraphQLEndpoint extends SimpleGraphQLServlet {

    private static final long serialVersionUID = -1296903382310297367L;

    @Autowired
    public GraphQLEndpoint(Query query) {
        super(SchemaParser.newParser()
                .file("employee.graphqls") //parse the schema file created earlier
                .resolvers(query)
                .build()
                .makeExecutableSchema());
    }

}
