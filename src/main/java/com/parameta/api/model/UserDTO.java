package com.parameta.api.model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO {

    private String name;
    private String lastName;
    private String documentId;
    private String documentType;
    private Date birthdate;
    private Date hiredate;
    private String position;
    private Integer salary;
    private String contractualPeriod;
    private String age;

    public UserDTO(String name, String lastName, String documentId, String documentType, 
            Date birthdate, Date hiredate, String position, Integer salary) {
        this.name = name;
        this.lastName = lastName;
        this.documentId = documentId;
        this.documentType = documentType;
        this.birthdate = new Date(birthdate.getTime());
        this.hiredate = new Date(hiredate.getTime());
        this.position = position;
        this.salary = salary;
    }
    
    public void setBirthdate(Date birthdate) {
        this.birthdate = new Date(birthdate.getTime());
    }

    public Date getBirthdate() {
        return new Date(birthdate.getTime());
    }

    public void setHiredate(Date hiredate) {
        this.hiredate = new Date(hiredate.getTime());
    }
    
    public Date getHiredate() {
        return new Date(hiredate.getTime());
    }
}

