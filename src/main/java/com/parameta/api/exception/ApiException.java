package com.parameta.api.exception;


import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class ApiException extends RuntimeException implements GraphQLError {

    private static final String CODE = "code";
    private static final String MESSAGE = "message";

    private StatusCodes code;

    public ApiException(Exception e) {
        super(StatusCodes.INTERNAL_ERROR.getText());
        log.error(this.getMessage(), e.getCause(), e);
        this.code = StatusCodes.INTERNAL_ERROR;
    }

    public ApiException() {
        super(StatusCodes.INTERNAL_ERROR.getText());
        log.error(this.getMessage(), this.getCause(), this);
        this.code = StatusCodes.INTERNAL_ERROR;
    }

    public ApiException(String message) {
        super(message);
        log.error(this.getMessage(), this.getCause(), this);

    }

    public ApiException(StatusCodes code) {
        super(code.getText());
        if (StatusCodes.INTERNAL_ERROR.getCode().equals(code.getCode())) {
            log.error(this.getMessage(), this.getCause(), this);
        }
        this.code = code;
    }

    @Override
    public Map<String, Object> getExtensions() {
        Map<String, Object> customAttributes = new LinkedHashMap<>();
        if (code == null) {
            customAttributes.put(CODE, StatusCodes.INTERNAL_ERROR.getCode());
            customAttributes.put(MESSAGE, StatusCodes.INTERNAL_ERROR.getText());
        } else {
            customAttributes.put(CODE, code.getCode());
            customAttributes.put(MESSAGE, code.getText());
        }
        return customAttributes;
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.DataFetchingException;
    }

    public StatusCodes getCode() {
        return code;
    }
}
