package com.parameta.api.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusCodes {
    SUCCESS("CIB-GEN-0200", "Success"),
    INTERNAL_ERROR("CIB-GEN-6666", "Internal Server error")
    ;


    private String code;
    private String text;
    
}
