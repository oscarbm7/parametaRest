package com.parameta.api.services;

import com.parameta.api.mapper.UserMapper;
import com.parameta.api.model.UserDTO;
import com.parameta.ws.client.EmployeeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    EmployeeClient client;
    
    public UserDTO employeeRegister(UserDTO userDto) {

        client.registerEmployee(UserMapper.MAPPER.toEmployeeDTO(userDto));
        return userDto;
    }

}
